﻿using UnityEngine;
using UnityEngine.UI;

namespace GameCore.Localization
{
    [RequireComponent(typeof(Image))]
    public class UILangImage : MonoBehaviour
    {
        [SerializeField]
        private string key;
        
        private Image image;

        private void Awake()
        {
            image = GetComponent<Image>();
            SetLocalizedSprite(Localizer.instance.Language);
        }

        private void Start()
        {
            Localizer.OnChangedLanguage += SetLocalizedSprite;
        }

        private void OnDestroy()
        {
            Localizer.OnChangedLanguage -= SetLocalizedSprite;
        }

        private void SetLocalizedSprite(LangCode code)
        {
            if (!string.IsNullOrEmpty(key))
            {
                image.sprite = Localizer.instance.GetSprite(key);
            }
            else
            {
                Debug.LogWarning("Key in the UILangImage component on " + gameObject.name + " is null or empty");
            }
        }
    }
}


