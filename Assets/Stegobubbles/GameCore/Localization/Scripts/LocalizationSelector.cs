﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameCore.Localization
{
    public class LocalizationSelector : MonoBehaviour
    {
        private Dropdown dropdown;

        private void Start()
        {
            if (dropdown == null) dropdown = GetComponent<Dropdown>();
            //Debug.Log(Localizer.instance.Language);
            //dropdown.options.Add(new Dropdown.OptionData(Localizer.deviceLang.ToString()));
            string[] listEnums = System.Enum.GetNames(typeof(LangCode));
            dropdown.options.Clear();
            for (int i = 0; i < listEnums.Length; i++)
            {
                string key = listEnums[i];
                dropdown.options.Add(new Dropdown.OptionData(key));
            }
            dropdown.onValueChanged.AddListener(new UnityAction<int>(ChangeValue));
            //Debug.Log((int)Localizer.instance.Language + " " + Localizer.instance.Language);
            dropdown.value = (int)Localizer.instance.Language;
            //Debug.LogError(Localizer.instance.Language);
        }

        public void ChangeValue(int i)
        {
            //Debug.Log(string.Format("{0} {1}", i, (LangCode)(i - 1)));
            Localizer.instance.SetLang((LangCode)(i));
        }
    }
}