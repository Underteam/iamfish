﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Game : MonoBehaviour
    {
        // Reference to the root instances of the MVC.
        public GameModel model;
        public GameView view;
        public GameController controller;
        public Achievement achievement;

        // Init things here
        void Start() { }
    }
}
