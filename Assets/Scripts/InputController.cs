﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class InputController : GameElement
    {
        [SerializeField]
        private Joystick LeftJoystick;
        [SerializeField]
        private Joystick MoveJoystick;

        public SwipeInput swipe;

        [NonSerialized]
        public bool cameraControl;

        public CameraController cameraController;

        public FishController fishController;

        void Start()
        {
            game.view.switchToCameraButton.onClick.AddListener(delegate { cameraControl = !cameraControl;});
            //game.view.switchControllerButton.onClick.AddListener(delegate
            //{
            //    game.model.PC = game.model.PC == GameModel.playerController.TailSwing ? GameModel.playerController.OneJoystick : GameModel.playerController.TailSwing;
            //});

            game.model.PC = GameModel.playerController.OneJoystick;
        }

        void Update()
        {
            //if (LeftJoystick.pressed && LeftJoystick.Direction.magnitude > 0.2f)
            //{
            //    SendMessageUpwards("OnLeftJoystickPressed", LeftJoystick.Direction, SendMessageOptions.DontRequireReceiver);
            //}
            //else
            //{
            //    SendMessageUpwards("OnLeftJoystickNotPressed", SendMessageOptions.DontRequireReceiver);
            //}

            if (swipe.input.magnitude > 0)
            {
                cameraController.Rotate(swipe.input);

                //fishController.Rotate(rotateAngle);
            }

            if ((MoveJoystick.pressed || Input.GetAxis("Horizontal") != 0) &&
                MoveJoystick.Direction.magnitude > 0.2f)
            {
                SendMessageUpwards("OnMoveJoystickPressed", MoveJoystick.Direction,
                    SendMessageOptions.DontRequireReceiver);
                //if (swipe.touched == true) swipe.touched = false;
            }
            else
            {
                SendMessageUpwards("OnMoveJoystickNotPressed", SendMessageOptions.DontRequireReceiver);
            }
        }

        public void JoystickReset()
        {
            MoveJoystick.OnPointerUp(null);
            SendMessageUpwards("OnMoveJoystickNotPressed", SendMessageOptions.DontRequireReceiver);
        }
    }
}
