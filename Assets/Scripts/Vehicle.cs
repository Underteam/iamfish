﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Vehicle : GameElement
    {
        private float speed = 9f;

        public GameObject[] wheels;

        private List<Transform> waypoints;
        private List<Transform> waypointsRight;
        private List<Transform> waypointsLeft;
        public bool isRightSide = true;
        private Transform targetPoint;
        public int index = 0;
        private bool looped = true;

        private Rigidbody rigidbody;

        void Start()
        {
            waypointsRight = game.controller.waypointsRight;
            waypointsLeft = game.controller.waypointsLeft;

            if (isRightSide) waypoints = waypointsRight;
            else waypoints = waypointsLeft;

            rigidbody = GetComponent<Rigidbody>();

            if (waypoints.Count < 1) return;

            targetPoint = waypoints[index];
        }

        private void Update()
        {
            Move();
            //WheelRotate();

            if (targetPoint == null) return;

            rigidbody.velocity = transform.forward * speed;
        }

        private void FixedUpdate()
        {
            //if (targetPoint == null) return;

            //rigidbody.velocity = transform.forward * speed;

            ////if (startMove)
            ////{
            ////    Move(targetPoint.position - transform.position);

            ////    CheckDistanceToTargetPoint();
            ////}
            ////if (finishPoint.position == transform.position) Destroy(gameObject);
        }

        private void WheelRotate()
        {
            if (targetPoint == null) return;

            for (int i = 0; i < wheels.Length; i++)
            {
                //wheels[i].transform.Rotate(speed/* * Time.deltaTime*/, 0, 0);
            }
        }

        private void Move()
        {
            if (targetPoint == null) return;
            Vector3 direction = targetPoint.position - transform.position;

            float distance = direction.magnitude;
            float delta = speed * Time.deltaTime;

            transform.forward =  Vector3.Lerp(transform.forward, direction.normalized, 0.25f * speed * Time.deltaTime);

            dir = Vector3.zero;
            //if (distance < delta + 0.1f)
            //{
            //    index = (index + 1) % waypoints.Count;
            //    targetPoint = waypoints[index];
            //}
            //else
            if (distance < 5)
            {
                //var nextPoint = waypoints[(index + 1) % waypoints.Count];
                //Vector3 direction2 = nextPoint.position - targetPoint.position;
                //transform.forward = Vector3.Lerp(direction.normalized, direction2.normalized, 1 - distance / 3);
                //dir = direction2.normalized;
                index = (index + 1) % waypoints.Count;
                targetPoint = waypoints[index];
            }
            //else
            //{

            //}
        }

        public Vector3 dir;
        //private void CheckDistanceToFrontVehicles()
        //{
        //    Debug.DrawRay(transform.position, transform.forward, Color.red);
        //    Physics.Raycast(transform.localPosition, transform.forward, out RaycastHit hit, 15f);
        //    if(hit.collider != null && hit.collider.gameObject.layer == 11)
        //    {
        //        float distance = Vector3.Distance(transform.position, hit.collider.transform.position);
        //        Debug.Log(distance);
        //    }
        //}

        private void TurnCarAround()
        {

        }
    }
}
