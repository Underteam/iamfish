﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPositionFixed : MonoBehaviour
{
    public Transform parent;
    private Vector3 position;

    public Rigidbody rb;

    public float a = 0;
    public float v = 0;
    public float x = 0;

    public float torque;

    public float amplitude = 30;

    private void Update()
    {
        position = parent.position;
        position.y += 0.00f;
        transform.position = position;

        v += a * Time.deltaTime;
        x += v * Time.deltaTime + a * Time.deltaTime * Time.deltaTime / 2;
        a = -10 * x - 0.5f * v - torque;

        transform.localRotation = Quaternion.Euler(180, 0, 0);

        Quaternion q;

        if (rb == null) return;
        q = Quaternion.AngleAxis(amplitude * x, rb.angularVelocity.normalized);
        transform.rotation *= q;
    }

    private Vector3 direction;
    private void FixedUpdate()
    {
        if (rb == null) return;
        direction = Vector3.Cross(rb.angularVelocity, Vector3.up);
        torque = rb.angularVelocity.magnitude;
    }

    private void OnDrawGizmos()
    {
        if (rb == null) return;
        Gizmos.color = Color.green;
        Gizmos.DrawRay(rb.transform.position, direction);
    }
}
