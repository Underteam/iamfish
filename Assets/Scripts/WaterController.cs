﻿using UnityEngine;

namespace Assets.Scripts
{
    public class WaterController : GameElement
    {
        public void Death()
        {
            Physics.Raycast(game.view.water.transform.position, Vector3.down, out var hit);

            for (int i = 0; i < 6; i++)
            {
                var randomIndex = Random.Range(0, game.view.puddle.Length);
                Instantiate(game.view.puddle[randomIndex], hit.point, Quaternion.Euler(-90,0,Random.Range(0,360)));
            }

            game.view.water.SetActive(false);
        }

        public void Alive()
        {
            game.view.water.SetActive(true);
        }
    }
}
