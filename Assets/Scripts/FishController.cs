﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FishController : GameElement
    {
        private Vector3 startDirection;
        private bool rotate;
        private Transform shitpoint;

        private bool dead;

        private void Start()
        {
            shitpoint = game.controller.shitPoint;
        }
        void Update()
        {
            if (dead)
                return;

            Move();
        }

        private void Move()
        {
            var distance = (game.view.fishView.transform.position - game.view.aquariumView.transform.position).magnitude;
            var distanceToTargetPoint = (game.model.fish_target_position - game.view.fishView.transform.position).magnitude;

            if (distanceToTargetPoint < 0.01f || distance >= (game.model.fish_target_position - game.view.aquariumView.transform.position).magnitude)
            {
                game.view.fishView.transform.position = game.model.fish_target_position;
            }
            else
            {
                game.view.fishView.transform.position = Vector3.MoveTowards(game.view.fishView.transform.position,
                    game.model.fish_target_position, Time.deltaTime * game.model.fish_move_speed);
            }
        }

        public void Rotate(float angle)
        {
            if (dead)
                return;

            if (!rotate)
            {
                Vector3 dir = game.view.cameraView.transform.forward;
                dir.y = 0;
                dir.Normalize();
                startDirection = dir;
                //startPosition = game.view.fishView.transform.forward;
                rotate = true;
            }
            var newDir = Vector3.RotateTowards(game.view.fishView.transform.forward,
                Quaternion.Euler(0,angle,0) * startDirection, Time.deltaTime * game.model.fish_rotate_speed, 0);

            game.view.fishView.transform.rotation = Quaternion.LookRotation(newDir);
        }

        public void ResetRotate()
        {
            if (rotate)
                rotate = false;
        }

        public void Death(bool isShit = false)
        {
            ResetRotate();
            if (isShit)
            {
                game.view.fishView.transform.position = shitpoint.position;
                game.view.fishView.transform.eulerAngles -= new Vector3(0, 0, 90);
                dead = true;
                return;
            }

            Physics.Raycast(game.view.water.transform.position, Vector3.down, out var hit);
            game.view.fishView.transform.position = hit.point;
            game.view.fishView.transform.eulerAngles -= new Vector3(0, 0, 90);
            dead = true;
        }

        public void Alive()
        {
            game.view.fishView.transform.eulerAngles = Vector3.zero;
            dead = false;
        }

        public bool DeathStatus()
        {
            return dead;
        }
    }
}
