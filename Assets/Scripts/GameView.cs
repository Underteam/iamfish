﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    // Contains all views related to the app.
    public class GameView : GameElement
    {
        public FishView fishView;
        public AquariumView aquariumView;
        public CameraView cameraView;
        public GameObject[] puddle;
        public GameObject water;

        public Button switchToCameraButton;
        public Button switchControllerButton;
        public Button restartButton;
    }
}
