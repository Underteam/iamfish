﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IDragHandler
{
    public float sens = 1;

    private Vector3 prevPos;
    private Vector3 curPos;

    private bool touched;
    private bool dragged;


    public Vector2 input { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (touched)
        {
            //Vector3 delta = Input.mousePosition - prevPos;
            Vector3 delta = curPos - prevPos;
            prevPos = curPos;
            delta.y = -delta.y;
            input = sens * 0.01f * delta;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.LogWarning("OnPointerDown");
        //curPos = Input.mousePosition;
        curPos = eventData.position;
        prevPos = curPos;
        touched = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //input = Vector2.zero;
        //touched = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.LogWarning("Up");
        input = Vector2.zero;
        touched = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //if (touched)curPos = eventData.position;
        curPos = eventData.position;
    }
}
