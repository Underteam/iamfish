﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositionChanger : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private GameObject target;

    private Vector3 hitPoint;
    private bool cameraClosed = false;

    private Vector3 startPos;
    private float defaultDistanceToCamera;

    private void Awake()
    {
        startPos = transform.position;
        //defaultDistanceToCamera = Vector3.Distance(transform.position, target.transform.position);
        defaultDistanceToCamera = 3;
    }

    void Update()
    {
        CheckCameraClosedByObject();
        if (cameraClosed && hitPoint != Vector3.zero) MoveCameraToPoint(target.transform.position - transform.position);

    }

    private void CheckCameraClosedByObject()
    {
        Ray ray = new Ray(target.transform.position, transform.position);
        float distance = 0;

        Physics.Raycast(ray, out RaycastHit hit);

        if (hit.transform != null)
        {
            hitPoint = hit.point;
            cameraClosed = true;
            Debug.LogError(hit.transform.gameObject.name, hit.transform.gameObject);

            distance = Vector3.Distance(hitPoint, target.transform.position);
        }

        if (distance >= defaultDistanceToCamera) cameraClosed = false;
    }

    private void MoveCameraToPoint(Vector3 point)
    {
        transform.Translate(point);
    }
}
