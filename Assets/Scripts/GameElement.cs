﻿using UnityEngine;

namespace Assets.Scripts
{
    // Base class for all elements in this application.
    public class GameElement : MonoBehaviour
    {
        // Gives access to the application and all instances.
        public Game game => GameObject.FindObjectOfType<Game>();
    }
}
