﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class GameController : GameElement
    {
        public List<Transform> waypointsRight;
        public List<Transform> waypointsLeft;

        public LoadPosition loadController;
        public FishController fishController;
        public AquariumController aquariumController;
        public InputController inputController;
        public CameraController cameraController;
        public WaterController waterController;
        public GameObject aquariumPref;
        public Transform shitPoint;

        //public GameObject leftJoystick;
        public GameObject moveJoystick;
        public GameObject menuPanel;
        public GameObject winPanel;

        [SerializeField] private GameObject currentAquarium;

        public Text FPS;
        public Text Timer;
        public Text info;

        private float movingForce;
        Object[] carList;

        public int flags = 0;
        public int smashedBowls = 0;
        private float gameTime = 0;

        private void Awake()
        {
            string status = PlayerPrefs.GetString("Status");

            if (status.Equals("NewGame")) { LoadNew(); }
            else if (status.Equals("LoadCheckpoint")) { LoadCheckPoint(); }
            else menuPanel.SetActive(true);
        }


        void Start()
        {
            //QualitySettings.vSyncCount = 0;
            //Application.targetFrameRate = -1;

            game.view.restartButton.onClick.AddListener(Restart);
            movingForce = aquariumController.rigidbody.mass * 55f;

            //RestartOnCheckpoint();


//#if UNITY_ANDROID
//            movingForce = aquariumController.rigidbody.mass * 3.6f;
//            Debug.LogError("Android " + movingForce);
//#endif
//            Debug.Log(movingForce); 
        }


        private float fps = 0;
        private float timer;
        private float final;
        private void Update()
        {
            fps++;
            timer += Time.deltaTime;
            gameTime += Time.deltaTime;

            if (timer >= 1) { final = fps / timer; fps = 0; timer = 0; }

            FPS.text = final.ToString();
            //Timer.text = ((int)gameTime).ToString();

            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if (menuPanel.active == false) MainMenuSetActive();
                else MainMenuInactive();
            }

            //string str = "Timer: " + (int)gameTime + ". Aquarium destroy " + smashedBowls;
            //info.text = str;
            //Debug.Log(str);
        }

        //public void OnViewJoystickPressed(Vector2 DirectionValue)
        //{
        //    var rotateAngle = 90 - Mathf.Atan2(DirectionValue.y, DirectionValue.x) * 180 / Mathf.PI;
        //    if (inputController.cameraControl || game.model.PC == GameModel.playerController.OneJoystick)
        //    {
        //        //cameraController.Rotate(rotateAngle);
        //        return;
        //    }

        //    //fishController.Rotate(rotateAngle);
        //}

        public void OnMoveJoystickPressed(Vector2 DirectionValue)
        {
            Vector3 dir = game.view.fishView.transform.forward;
            dir.y = 0;
            dir.Normalize();
            aquariumController.AddForce(dir * DirectionValue.magnitude * movingForce * Time.deltaTime);

            var rotateAngle = 90 - Mathf.Atan2(DirectionValue.y, DirectionValue.x) * 180 / Mathf.PI;

            fishController.Rotate(rotateAngle);

            game.model.fish_target_position = game.view.aquariumView.transform.position + dir * 0.2f;
        }

        public void OnMoveJoystickNotPressed()
        {
            game.model.fish_target_position = game.view.aquariumView.transform.position;
            fishController.ResetRotate();
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void DestoyAquarium(bool shitCase = false)
        {
            cameraController.DeathFocus();
            Debug.Log("DestroyBowl");
            waterController.Death();
            aquariumController.Death();
            fishController.Death(shitCase);
            smashedBowls++;
            MainMenuSetActive();
            game.controller.inputController.JoystickReset();
            game.view.aquariumView.ColliderOff();
        }

        public void RegenerateAquarium()
        {
            game.controller.inputController.JoystickReset();
            waterController.Alive();
            aquariumController.Alive();
            fishController.Alive();
            aquariumController.rigidbody.velocity = Vector3.zero;
            game.view.aquariumView.SetAlive();
        }

        public void ClearAndRestart()
        {
            DefaultSave();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //PlayerPrefs.SetString("Status", "NewGame");
        }

        //public void CheckPoint()
        //{
        //    Save();
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //}

        public void LoadCheckPoint()
        {
            if (game.view.aquariumView.haveCheckpoint == false) { Debug.LogError("Not have checkpoints!"); /*PlayerPrefs.DeleteAll();*/ PlayerPrefsDeleteSaves(); return; }

            Save();
            RegenerateAquarium();
            loadController.LoadCheckpoint();
            MainMenuInactive();
            cameraController.GetStart();
            //PlayerPrefs.DeleteAll();
            PlayerPrefsDeleteSaves();
        }

        public void Save()
        {
            PlayerPrefs.SetFloat("X", game.view.aquariumView.currentStartPosition.x);
            PlayerPrefs.SetFloat("Y", game.view.aquariumView.currentStartPosition.y);
            PlayerPrefs.SetFloat("Z", game.view.aquariumView.currentStartPosition.z);

            PlayerPrefs.SetString("Status", "LoadCheckpoint");
            PlayerPrefs.SetFloat("Timer", gameTime);
            PlayerPrefs.SetInt("SmashedBowls", smashedBowls);
            PlayerPrefs.SetInt("Flags", flags);
        }

        private void DefaultSave()
        {
            Vector3 dafPos = new Vector3(7f, 4.45f, -18.7f);
            PlayerPrefs.SetFloat("X", dafPos.x);
            PlayerPrefs.SetFloat("Y", dafPos.y);
            PlayerPrefs.SetFloat("Z", dafPos.z);

            //PlayerPrefs.SetString("Status", "NewGame");
            PlayerPrefs.SetFloat("Timer", 0);
            PlayerPrefs.SetInt("SmashedBowls", 0);
            PlayerPrefs.SetInt("Flags", 0);
        }

        private void MainMenuSetActive()
        {
            menuPanel.SetActive(true);
            moveJoystick.SetActive(false);
        }

        private void MainMenuInactive()
        {
            menuPanel.SetActive(false);
            moveJoystick.SetActive(true);
        }

        public void NewGame()
        {
            PlayerPrefs.SetString("Status", "NewGame");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void LoadNew()
        {
            menuPanel.SetActive(false);
            moveJoystick.SetActive(true);
            currentAquarium.SetActive(true);

            cameraController.GetStart();
            //PlayerPrefs.DeleteAll();
            PlayerPrefsDeleteSaves();
        }

        public void Win()
        {
            if(gameTime <= 120) game.achievement.CompleteAcievement(Achievement.AchieveType.FastEscape);

            if(smashedBowls == 0) game.achievement.CompleteAcievement(Achievement.AchieveType.SaveAquarium);
            else if(smashedBowls >= 10) game.achievement.CompleteAcievement(Achievement.AchieveType.TenAquariums);

            Debug.Log("Win");
            MainMenuInactive();
            winPanel.SetActive(true);
            //PlayerPrefs.DeleteAll();
            PlayerPrefsDeleteSaves();
        }

        private void PlayerPrefsDeleteSaves(bool fullReset = false)
        {
            if (fullReset)
            {
                PlayerPrefs.DeleteAll();
                return;
            }

            PlayerPrefs.DeleteKey("Status");
            PlayerPrefs.DeleteKey("X");
            PlayerPrefs.DeleteKey("Y");
            PlayerPrefs.DeleteKey("Z");

            PlayerPrefs.DeleteKey("Timer");
            PlayerPrefs.DeleteKey("SmashedBowls");
            PlayerPrefs.DeleteKey("Flags");
        }

        public void FullReset()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
