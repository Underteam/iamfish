﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Returner : MonoBehaviour
{
    public List<Transform> objects;
    public List<Transform> parents;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    [EditorButton]
    public void Save()
    {
        var list = GetComponentsInChildren<Transform>(true);

        for(int i = 0; i < list.Length; i++)
        {
            objects.Add(list[i]);
            parents.Add(list[i].parent);
        }
    }

    [EditorButton]
    public void Load()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].SetParent(parents[i]);
        }
    }
}
