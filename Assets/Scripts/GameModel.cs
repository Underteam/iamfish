﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameModel : GameElement
    {
        public float fish_rotate_speed;
        public float fish_move_speed;
        public Vector3 fish_target_position;

        public float camera_rotate_speed;

        public enum playerController
        {
            TailSwing, OneJoystick
        }

        public playerController PC = playerController.TailSwing;

        public float relativeVelocityToDestroyAquarium;
    }
}
