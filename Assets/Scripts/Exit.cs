﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Exit : GameElement
    {
        public void ExitGame()
        {
            PlayerPrefs.SetString("Status", "Default");
            Application.Quit();
        }
    }
}
