﻿using UnityEngine;

namespace Assets.Scripts
{
    public class AquariumController : GameElement
    {
        public Rigidbody rigidbody;
        public bool onAirAfterBounce = false;
        private float prevSwingValue;
        private float deltaSwing;
        private float maxVelocity = 2;

        void Start()
        {
            rigidbody = game.view.aquariumView.GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (rigidbody == null) return;
            if (rigidbody.velocity.magnitude > 0 && !onAirAfterBounce)
            {
                if(!game.view.aquariumView.onRamp) /*rigidbody.velocity -= rigidbody.velocity * 0.005f; */ rigidbody.velocity -= rigidbody.velocity * 0.4f * Time.deltaTime;
                else
                {
                    if (rigidbody.velocity.magnitude <= 2) rigidbody.velocity += rigidbody.velocity * 1.25f * Time.deltaTime;
                }
            }
        }

        public void Swing(float swingValue)
        {
            if (prevSwingValue * swingValue > 0)
                return;

            deltaSwing = Mathf.Abs(prevSwingValue - swingValue);
            prevSwingValue = swingValue;

            var force = game.view.fishView.transform.forward * deltaSwing;
            AddForce(force);
        }

        public void AddForce(Vector3 force)
        {
            var point = game.view.aquariumView.transform.position + force.normalized * 0.5f;
            if(rigidbody.velocity.magnitude <= maxVelocity || onAirAfterBounce) rigidbody.AddForceAtPosition(force, point);
        }

        public void Death()
        {
            game.view.aquariumView.GetComponent<Rigidbody>().isKinematic = true;
            game.view.aquariumView.GetComponent<BreakableWindow>().breakWindow();
            game.view.aquariumView.prop.gameObject.SetActive(false);
        }

        public void Alive()
        {
            game.view.aquariumView.GetComponent<Rigidbody>().isKinematic = false;
            //game.view.aquariumView.GetComponent<BreakableWindow>().breakWindow();
            game.view.aquariumView.prop.gameObject.SetActive(true);
        }
    }
}
