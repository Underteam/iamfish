﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Collider))]
public class Bouncer : MonoBehaviour
{
    // Start is called before the first frame update

    Animator anim = null;
    ParticleSystem bounceParticles = null;

    private void Awake()
    {
        //gameObject.layer = 10;
        CachedCollider.isTrigger = true;

        anim = GetComponent<Animator>();
        bounceParticles = GetComponentInChildren<ParticleSystem>();
    }

    //private void Start () {
    //    if (Application.isPlaying)
    //        playerController = PlayerController.instance;
    //}

    /// <summary>
    /// Нахождение положения точки при движении под углом к горизонту
    /// </summary>
    /// <param name="pos0">Начальная позиция</param>
    /// <param name="speed0">Начальная скорость</param>
    /// <param name="t">Момент времени</param>
    /// <param name="angle">Угол</param>
    /// <param name="gravity">Сила гравитации</param>
    /// <returns></returns>
    public float rotation = 0;
    public static Vector3 CalcPoint(Vector3 start, Vector3 startVelocity, float time)
    {
        return start + startVelocity * time + Physics.gravity * time * time * 0.5f;
    }

    public static List<Vector3> CalcPath(Transform transform, Vector3 pos0, Vector3 startVelocity, float time, float period, bool breakOnCollision = true)
    {

        List<Vector3> resultPoints = new List<Vector3>();

        for (float timer = 0; timer < time; timer += period)
        {
            resultPoints.Add(CalcPoint(pos0, startVelocity, timer));
            if (breakOnCollision)
                if (resultPoints.Count > 1)
                {
                    RaycastHit hit;
                    if (Physics.Linecast(resultPoints[resultPoints.Count - 2], resultPoints[resultPoints.Count - 1], out hit))
                    {
                        if (hit.transform == transform || hit.transform.gameObject.layer == 8 || hit.transform.gameObject.layer == 9 || hit.transform.gameObject.layer == 20)
                            continue;

                        // Bouncer anotherBouncer = hit.transform.GetComponent<Bouncer> ();
                        // if (anotherBouncer != null) {
                        //     anotherBouncer.DrawPath (hit.point);
                        // }
                        return resultPoints;
                    }
                }
        }

        return resultPoints;
    }

    [HideInInspector]
    public int debugBounceIndex = 0;

    [HideInInspector]
    public int bounceIndex = 0;

    [Header("Лист отскоков")]
    public List<Bounce> bounces = new List<Bounce>();
    public bool infinityBounces = true;

    [System.Serializable]
    public class Bounce
    {

        [Header("Придаваемая скорость")]
        public float givenSpeed = 10;

        [Header("Выходной угол")]
        public float angle = 45;

        public Vector3 GetAngleVector(float rotation)
        {
            return Quaternion.Euler(0, rotation, 0) * Bouncer.GetAngleVector(angle);
        }
    }

    public static Vector3 GetAngleVector(float angle)
    {
        return (Quaternion.Euler(0, 0, angle) * Vector3.right).normalized;
    }

    [Header("Брать угол из поворота обьекта")]
    public bool getAngleFromRotation = false;

    //public LevelGenerator levelGenerator;

    // [HideInInspector]
    //public PlayerController playerController = null;

    //private void OnTriggerEnter (Collider other) {
    //    if (this == levelGenerator.startBouncer)
    //        return;

    //    if (other.gameObject.layer == 12)
    //        levelGenerator.playerController.Fall (this);

    //    //если таймер не позволяет
    //    if (!canKick || playerController == null)
    //        return;

    //    //если персонаж уже падает и это не тот батут, который его убил или персонаж уже мёртв
    //    if ((playerController.IsFalling && playerController.bouncerFallOn != this) || playerController.IsDead)
    //        return;

    //    PlayerController player = other.GetComponentInParent<PlayerController> ();
    //    if (player != null) {
    //        if (other.gameObject.layer == 9)
    //            Kick (player, other.gameObject.GetComponent<Collider> ().ClosestPointOnBounds (transform.position));

    //        if (player.IsFlipping)
    //            player.Fall (this);
    //    }
    //}

    //public bool canKick {
    //    get {
    //        return inactiveTimer <= 0 && GameController.instance.IsGameStarted;
    //    }
    //}

    //float inactiveTimer = 0;
    //public IEnumerator InactiveCoroutine () {
    //    inactiveTimer = levelGenerator.bouncerInactiveTime;
    //    while (inactiveTimer > 0) {
    //        inactiveTimer -= Time.deltaTime;
    //        yield return null;
    //    }
    //}

    //public void Kick (PlayerController player, Vector3 collisionPoint) {

    //    Rigidbody rigid = player.baseRigidbody;
    //    // CapsuleCollider playerCollider = player.baseCollider;
    //    // player.Bounce ();

    //    if (bounceIndex < bounces.Count) {

    //        Vector3 velocity = bounces[bounceIndex].GetAngleVector ().normalized * bounces[bounceIndex].givenSpeed;

    //        // Bounds colliderBounds = new Bounds (playerCollider.transform.TransformPoint (playerCollider.bounds.center), new Vector3 (playerCollider.radius, playerCollider.height, playerCollider.radius));
    //        float angularPower = (collisionPoint.x - player.baseRigidbody.transform.position.x) / (player.height / 2) * 30;
    //        // Debug.LogError ("bounds " + colliderBounds + " (collisionPoint.x - colliderBounds.center.x) = " + (collisionPoint.x - colliderBounds.center.x));
    //        player.PuppetKick (velocity, bounces[bounceIndex].givenSpeed * Mathf.Abs (angularPower) * ((collisionPoint.x > player.baseRigidbody.transform.position.x) ? -1 : 1));

    //        if (anim != null)
    //            anim.SetTrigger ("Hit");

    //        if (bounceParticles != null)
    //            bounceParticles.Play ();

    //        bounceIndex++;
    //    }

    //    StartCoroutine (InactiveCoroutine ());
    //}

    //private void OnValidate()
    //{
    //    CheckCapability();
    //}

    Collider cachedCollider = null;
    public Collider CachedCollider
    {
        get
        {
            if (cachedCollider == null)
            {
                cachedCollider = GetComponentInChildren<Collider>();
            }

            return cachedCollider;
        }
    }


    public float debugTime;
    public float debugPeriod;

    public void DrawPath(Vector3 startPos)
    {
        //if (levelGenerator == null)
        //    return;

        if (bounces.Count <= 0 || debugBounceIndex + 1 >= bounces.Count)
            return;

        debugBounceIndex++;

        Bounds colliderBounds = CachedCollider.bounds;
        //heckCapability();
        List<Vector3> path = CalcPath(transform, startPos, bounces[debugBounceIndex].GetAngleVector(rotation) * bounces[debugBounceIndex].givenSpeed, debugTime, debugPeriod);
        DrawPathList(path);
    }

    public static void DrawPathList(List<Vector3> path)
    {
        for (int i = 1; i < path.Count; i++)
        {
            Debug.DrawLine(path[i - 1], path[i], Color.red);
            DrawArrow(path[i - 1], path[i] - path[i - 1], 0.2f, Color.green, 20, 0.2f);
        }
    }

    public static void DrawArrow(Vector3 start, Vector3 dir, float length, Color color, float angle = 20, float endLength = 2)
    {

        Debug.DrawRay(start, dir * length, color);

        Vector3 end = start + dir * length;

        Debug.DrawRay(end, Quaternion.AngleAxis(angle, Vector3.forward) * (start - end).normalized * endLength, color);
        Debug.DrawRay(end, Quaternion.AngleAxis(-angle, Vector3.forward) * (start - end).normalized * endLength, color);
    }

    //void OnDrawGizmos () {
    //    Gizmos.DrawIcon (new Vector3 (CachedCollider.bounds.center.x, CachedCollider.bounds.max.y, CachedCollider.bounds.center.z), "batoot", true);
    //}

#if UNITY_EDITOR
    private void Update()
    {
        debugBounceIndex = -1;
        Vector3 startPos = new Vector3(transform.position.x, CachedCollider.bounds.max.y, transform.position.z);
        DrawPath(startPos);
        //if (debugBounceIndex >= 0)
        //    DrawArrow(startPos, bounces[debugBounceIndex].GetAngleVector(rotation), 5, Color.cyan);
    }
#endif
}