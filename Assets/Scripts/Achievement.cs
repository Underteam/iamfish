﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Achievement : GameElement
    {
        public Text[] achieves;

        public enum AchieveType
        {
            EscapeByTruck,
            EscapeByPickup,
            EscapeByTreesHouse,
            TrampolineUse,
            CorniceUse,
            JumpToThePillow,
            GoToOcean,
            GoToKitchen,
            SaveAquarium,
            KilledByVehicle,
            FastEscape,
            TenAquariums
        }

        private Color color;
        private void Start()
        {
            color = Color.yellow;
            AchievementTextUpdate();
        }

        public void CompleteAcievement(AchieveType type)
        {
            PlayerPrefs.SetInt("Achieve_" + (int)type, 1);
            //Debug.Log("Achieve_" + type);
            AchievementTextUpdate();

        }

        //public void LoadAchieves()
        //{
        //    for (int i = 0; i < achieves.Length; i++)
        //    {
        //        if (PlayerPrefs.GetInt("Achieve_" + i) == 1)
        //    }
        //}

        private void AchievementTextUpdate()
        {
            for (int i = 0; i < achieves.Length; i++)
            {
                if (PlayerPrefs.GetInt("Achieve_" + i) == 1) achieves[i].color = Color.black;
                else achieves[i].color = color;
            }
        }

        public void AchievesClear()
        {
            for (int i = 0; i < achieves.Length; i++)
            {
                PlayerPrefs.SetInt("Achieve_" + i, -1);
            }

            AchievementTextUpdate();
        }
    }
}
