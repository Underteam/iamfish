﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class CheckPoint : GameElement
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<AquariumView>() != null)
            {
                //Debug.Log("NewCheckPoint");
                other.GetComponent<AquariumView>().SetNewCheckpoint();
                game.controller.flags++;
                Destroy(gameObject);
            }
        }
    }
}
