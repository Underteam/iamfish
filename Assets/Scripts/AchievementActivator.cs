﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class AchievementActivator : GameElement
    {
        [SerializeField] private Achievement.AchieveType achieveType;

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<AquariumView>() != null)
            {
                game.achievement.CompleteAcievement(achieveType);
                if (achieveType == Achievement.AchieveType.EscapeByPickup)
                {
                    other.transform.SetParent(transform);
                    Destroy(other.GetComponent<Rigidbody>());
                    game.controller.Win();
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.GetComponent<AquariumView>() != null)
            {
                game.achievement.CompleteAcievement(achieveType);
            }
        }
    }
}