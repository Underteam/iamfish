﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class LoadPosition : GameElement
    {
        private void Start()
        {
            Load();
        }

        private void Load()
        {
            Vector3 dafPos = new Vector3(7f, 4.45f, -18.7f);
            PlayerPrefs.SetFloat("X", dafPos.x);
            PlayerPrefs.SetFloat("Y", dafPos.y);
            PlayerPrefs.SetFloat("Z", dafPos.z);

            game.view.aquariumView.currentStartPosition.x = PlayerPrefs.GetFloat("X");
            game.view.aquariumView.currentStartPosition.y = PlayerPrefs.GetFloat("Y");
            game.view.aquariumView.currentStartPosition.z = PlayerPrefs.GetFloat("Z");

            transform.localPosition = game.view.aquariumView.currentStartPosition;
            game.view.aquariumView.transform.position = transform.position;
        }


        public void LoadCheckpoint()
        {
            game.view.aquariumView.currentStartPosition.x = PlayerPrefs.GetFloat("X");
            game.view.aquariumView.currentStartPosition.y = PlayerPrefs.GetFloat("Y");
            game.view.aquariumView.currentStartPosition.z = PlayerPrefs.GetFloat("Z");

            transform.localPosition = game.view.aquariumView.currentStartPosition;
            game.view.aquariumView.transform.position = transform.position;
        }
    }
}