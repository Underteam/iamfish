﻿using UnityEngine;

namespace Assets.Scripts
{
    public class AquariumView : GameElement
    {
        public Bouncer bouncer;
        public Transform prop;
        private bool destroyPossible = true;
        private bool alive = true;
        public bool onRamp = false;
        public bool doBounce = false;

        public bool haveCheckpoint = false;
        public Vector3 currentStartPosition;

        private SphereCollider sphereCollider;
        private Rigidbody rigBody;

        private void Awake()
        {
            rigBody = GetComponent<Rigidbody>();
            sphereCollider = gameObject.AddComponent<SphereCollider>();
        }


        void OnCollisionEnter(Collision collision)
        {
            if (!alive) return;
            game.controller.aquariumController.onAirAfterBounce = false;
            if (collision.gameObject.layer == 9) { destroyPossible = false; Invoke("SetDestroyPossibleTrue", 0.1f); }
            else if (collision.gameObject.layer == 10) { onRamp = true; /*Debug.Log("Onramp " + onRamp);*/ }
            else if (collision.gameObject.layer == 11)
            {
                Debug.LogError("VehicleKillYou!");
                destroyPossible = true;
                game.controller.DestoyAquarium();
                alive = false;
                game.achievement.CompleteAcievement(Achievement.AchieveType.KilledByVehicle);
                return;
            }
            else if(collision.gameObject.layer == 16) 
            {
                GameObject escapeTrack = collision.gameObject;
                transform.SetParent(escapeTrack.transform);
                Destroy(rigBody);
                //rigBody.useGravity = false;
                //rigBody.isKinematic = true;
                game.controller.Win();
                game.achievement.CompleteAcievement(Achievement.AchieveType.EscapeByTruck);
                return;
            }
            else
            {
                if(!destroyPossible) Invoke("SetDestroyPossibleTrue", 0.1f);
                //destroyPossible = true;
                onRamp = false;
            }

            //Debug.Log("CollName " + collision.collider.name + " " + (collision.relativeVelocity.magnitude > game.model.relativeVelocityToDestroyAquarium) + " DestroyPossible " + destroyPossible + " collision " + collision.gameObject.name);

            if (destroyPossible && alive && (collision.relativeVelocity.magnitude > game.model.relativeVelocityToDestroyAquarium))
            {
                game.controller.DestoyAquarium();
                alive = false;
            }
            else if (collision.gameObject.layer == 12) { game.controller.DestoyAquarium(true); alive = false; }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Bouncer")
            {
                //Vector3 startPos = other.gameObject.transform.position;
                //startPos.y += 0.5f;
                //transform.position = startPos;
                rigBody.velocity = bouncer.bounces[0].GetAngleVector(50).normalized * bouncer.bounces[0].givenSpeed;
                game.controller.aquariumController.onAirAfterBounce = true;
                game.achievement.CompleteAcievement(Achievement.AchieveType.TrampolineUse);
            }
            else if (other.gameObject.layer == 15) { game.controller.Win(); game.achievement.CompleteAcievement(Achievement.AchieveType.GoToOcean); }
            else game.controller.aquariumController.onAirAfterBounce = false;
        }

        public void SetNewCheckpoint()
        {
            //Debug.Log("SetNewpos", this);
            currentStartPosition = transform.position;
            game.controller.Save();
            haveCheckpoint = true;
        }

        private void SetDestroyPossibleTrue()
        {
            destroyPossible = true;
        }

        public void SetAlive()
        {
            sphereCollider.enabled = true;
            alive = true;
        }

        public void ColliderOff()
        {
            sphereCollider.enabled = false;
        }
    }
}
