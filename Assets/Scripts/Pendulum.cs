﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    Rigidbody rigBody;
    float timer;
    float randomForce;

    void Start()
    {
        rigBody = GetComponent<Rigidbody>();
        timer = Random.Range(3, 15f);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            randomForce = Random.Range(15, 30);
            //randomForce = 30;
            randomForce *= rigBody.mass / 10;
            rigBody.AddForce(transform.forward * randomForce, ForceMode.Impulse);
            timer = Random.Range(3, 15f);

        }
    }
}
