﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class CameraController : GameElement
    {
        private bool start = false;
        private bool rotateDeathCam = false;
        //private bool start = true;
        public Transform target;

        public float distance;

        private Vector3 direction = new Vector3(0, 1, 1);

        private void Start()
        {
            direction.Normalize();
        }

        private void FixedUpdate()
        {
            if (rotateDeathCam) RotateDethCamera();
            if (target == null || !start) return;

            Vector3 targetPosition = target.position + direction * distance;

            RaycastHit hit;
            if (Physics.Raycast(target.position, direction, out hit, distance))
            {
                if (hit.distance > 0.3f) targetPosition = target.position + direction * (hit.distance - 0.2f);
                else targetPosition = target.position + Vector3.up;
            }

            transform.position = Vector3.Lerp(transform.position, targetPosition, 20 * Time.deltaTime);

            transform.rotation =  Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-direction), 20 * Time.deltaTime);
        }

        public void Rotate(Vector2 input)
        {
            direction = Quaternion.AngleAxis(input.x, Vector3.up) * direction;

            Vector3 perp = Vector3.Cross(direction, Vector3.up);

            float angle = 90 - Vector3.SignedAngle(direction, Vector3.up, perp);

            A = angle;

            if (angle + input.y > 89) input.y = 89 - angle;
            if (angle + input.y < 20) input.y = 20 - angle;

            direction = Quaternion.AngleAxis(input.y, perp) * direction;

            direction.Normalize();
        }

        public float A;

        public void GetStart()
        {
            start = true;
            rotateDeathCam = false;
        }

        private Vector3 shiftPos = new Vector3(-1f, 1.5f, -1f);
        //private Vector3 shiftPos = new Vector3(-2f, 1.5f, -2f);
        public void DeathFocus()
        {
            start = false;
            Vector3 dethCamPos = game.view.fishView.transform.position + shiftPos;
            transform.position = dethCamPos;
            transform.LookAt(game.view.fishView.transform.position);
            Invoke("StartRotateDethCam", 0.1f);
        }

        private void StartRotateDethCam() { rotateDeathCam = true; }
        private void RotateDethCamera()
        {
            transform.RotateAround(game.view.fishView.transform.position, Vector3.up, 30 * Time.deltaTime);
        }
    }
}
